import sys
import datetime
import argparse
import subprocess
import matplotlib.pyplot as plt
import numpy as np
from multiprocessing import Pool

from matplotlib.patches import Rectangle
from matplotlib.collections import PatchCollection


def base_run(prgm, args):
    nargs = len(args)
    launcharg = [prgm]
    if nargs > 2:
        n, nrun, ntas = args[:3]
        launcharg.extend([str(n), str(nrun), str(ntas)])
    elif nargs == 2:
        n, nrun = args
        launcharg.extend([str(n), str(nrun)])
    else:
        raise ValueError('Not enough argument (should be 2 or 3)')

    p = subprocess.Popen(launcharg, stdout=subprocess.PIPE)
    out, _ = p.communicate()

    out = out.decode('ascii', 'ignore')
    return out


def rectPatches(distrib):
    patches = []
    for i in range(distrib.shape[0]):
        patches.append(Rectangle((i, 0), 1, distrib[i]))

    return PatchCollection(patches, facecolor='b', edgecolor='k')


def run(args):
    output = base_run('./allumettes.exe', args)
    lines = output.split('\n', 1)

    suite = np.array([int(u) for u in lines[0].strip().split()])
    return suite


def run_one(args):
    output = base_run('./allumettes_one.exe', args)
    lines = output.split('\n')

    process = []
    for l in lines:
        process.append(np.array([int(u) for u in l.strip().split()]))
    return process


def main_one(n, nrun):
    nrun = 3000
    n = 50
    nkeep = n + 20

    steps = run_one((n, nrun))

    fig = plt.figure()
    ax = fig.add_subplot(111)

    plt.show(False)
    plt.draw()

    for r in steps:
        ax.clear()
        ax.add_collection(rectPatches(r))
        ax.set_xlim([0, nkeep])
        ax.set_ylim([0, 100])
        fig.canvas.draw()


def main(n, nrun):
    pool = Pool(4)

    nrun = 3000
    n = 50

    # results = pool.imap(run, ((n, nrun) for i in range(1000)))
    results = pool.imap(run, ((n, nrun, i+10) for i in range(10)))
    # results = pool.imap(run, ((n, nrun, i) for i in [1]))

    ax = plt.figure().add_subplot(111)
    for r in results:
        ax.plot(r)

    ax.grid()

    plt.show()


if __name__ == '__main__':
    argd = argparse.ArgumentParser()
    argd.add_argument('n', help='entier n, genere n*(n+1)/2 allumettes')
    argd.add_argument('nrun', help='nombre de pass')
    argd.add_argument('--onerun', action='store_true')

    args = argd.parse_args()

    with open('history_call.txt', 'a') as f:
        txt = "{0:%Y-%m-%d %H:%M:%S} : {1}\n".format(
            datetime.datetime.now(),
            ' '.join(sys.argv))
        f.write(txt)

    if args.onerun:
        main_one(args.n, args.nrun)
    else:
        main(args.n, args.nrun)
