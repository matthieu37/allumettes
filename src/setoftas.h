#ifndef SETOFTAS_HEADER
#define SETOFTAS_HEADER

#include <vector>
#include <iostream>

class SetOfTas {
    public:
        typedef int value;
        typedef std::vector<value> container;
        typedef std::vector<size_t> run_container;
    private:
        container _setup;
        run_container _serie;
        int _totalAllumettes;

        void add_element();
        void sort();

    public:
        SetOfTas(int n);

        bool init_random();
        bool init_uniform(int ntas);

        void reduce();
        void operate();
        container get_distribution() const;
        void write_distribution(std::ostream& os) const;

        size_t getNTas() const;

        void run(int nrun, run_container& res);
        void run(int nrun, std::ostream& os);
};

#endif