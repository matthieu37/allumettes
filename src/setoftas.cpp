#include <chrono>
#include <random>
#include <vector>
#include <iostream>
#include <algorithm>
#include <iterator>


#include "setoftas.h"

using namespace std;

SetOfTas::SetOfTas(int n) :
    _totalAllumettes(n)
{
    _setup.reserve(n);
    _serie.reserve(n);
}

void SetOfTas::add_element() {
    _serie.push_back(getNTas());
}

bool SetOfTas::init_random() {
    _serie.clear();
    _setup.clear();
    _setup.resize(_totalAllumettes, 0);

    auto seed = chrono::high_resolution_clock::now().time_since_epoch().count();

    std::mt19937 r(seed);
    std::default_random_engine e1(r());
    std::uniform_int_distribution<> gen(1, _totalAllumettes);
    //std::mt19937 gen(r());

    for(int i = 0; i<_totalAllumettes; ++i) {
        int itas = gen(e1)-1;
        _setup[itas] += 1;
    }
    reduce();

    //add_element();
    return true;
}

void SetOfTas::sort() {
    std::sort(std::begin(_setup), std::end(_setup), std::greater<value>());
}

bool SetOfTas::init_uniform(int ntas) {
    _setup.clear();
    if(ntas <= 0)
        return false;

    _setup.reserve(ntas);

    int v = _totalAllumettes/ntas,
        ajustement = _totalAllumettes - v*ntas;

    for(int i= 0; i<ntas; i++)
        _setup.push_back(v);
    
    _setup.back() += ajustement;

    return true;
}

void SetOfTas::reduce() {
    container newv(_setup);

    _setup.clear();

    for(auto v: newv)
        if(v>0)
            _setup.push_back(v);
}

void SetOfTas::operate() {
    value count=0;
    for(auto &v: _setup){
        if(v>0) {
            v--;
            count+=1;
        }
    }
    _setup.push_back(count);

    reduce();
    //add_element();
}

SetOfTas::container SetOfTas::get_distribution() const {
    return _setup;
}

void SetOfTas::write_distribution(ostream& os) const {
    for(const auto value: _setup) {
        os << ' ' << value;
    }
    os << endl;
}


size_t SetOfTas::getNTas() const {
    return _setup.size();
}

void SetOfTas::run(int nrun, SetOfTas::run_container& res) {
    res.clear();
    res.reserve(nrun);

    for(int i=0; i<nrun; ++i) {
        operate();
        res.push_back(getNTas());
    } 
}

void SetOfTas::run(int nrun, ostream& of) {
    for(int i=0; i<nrun; ++i) {
        operate();
        sort();
        write_distribution(of);
    } 
}