#include <iostream>
#include <fstream>
#include <iterator>

#include "setoftas.h"

using namespace std;

template<typename Vector, typename OStream>
void write_vector(const Vector &vector, OStream &os) {
    for(const auto value: vector)
        os << ' ' << value;
    os << endl;
}

int main(int argc, char* argv[]) {
    if(argc < 3) {
        cerr << "not enough arguments !" << endl;
        cerr << argv[0] << " n nrun [ntas]" << endl;
        return EXIT_FAILURE;
    }

    int n = stoi(argv[1]);
    int nallumettes = n*(n+1)/2;
    int nrun = stoi(argv[2]);

    SetOfTas test(nallumettes);

    if(argc > 3) {
        int ntas = stoi(argv[3]);
        test.init_uniform(ntas);
    } else {
        test.init_random();
    }

    SetOfTas::run_container vrun;

    test.run(nrun, vrun);
    write_vector(vrun, cout);

    return EXIT_SUCCESS;
}