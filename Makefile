.PHONY: all clean

CXX=g++
CXXFLAGS=-O2 -Wall
LFLAGS=

SRC=src
EXEC=allumettes allumettes_one

all: $(EXEC)

%.o:%.cpp
	$(CXX) -o $@ -c $< $(CXXFLAGS)

allumettes: $(SRC)/main.o $(SRC)/setoftas.o

allumettes_one: $(SRC)/main_onerun.o $(SRC)/setoftas.o

$(EXEC):
	$(CXX) -o $@ $^ $(LFLAGS)

clean:
	rm -f $(SRC)/*.o $(EXEC)